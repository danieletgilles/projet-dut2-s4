package pages;

import static common.AppConstants.PLAYLIST;
import static common.AppConstants.USER;

import java.util.List;
import java.util.Map;

import model.MusicFan;
import model.PlayList;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import persistence.PersistenceService;

import common.Session;
import common.SessionManager;

import freemarker.template.Configuration;

public class HomePage extends AbstractPageHandler {

	@Override
	public String handle(Configuration cfg, Map<String, Object> context,
			Request request, Response response) {
		Session session = SessionManager.getSession(request, response);
		MusicFan user = session.getAttribute(USER);
		findTracks(user, context, session);
		findOtherPlayLists(user, context);
		return handle(cfg, context, request, response, "first.ftl");
	}

	private void findTracks(MusicFan user, Map<String, Object> context,
			Session session) {
		assert user != null;
		PlayList playlist = session.getAttribute(PLAYLIST);
		if (playlist == null) {
			playlist = user.getPlaylists().get(0);
			session.setAttribute(PLAYLIST, playlist);
		}
		context.put("playlist", playlist);
	}

	private void findOtherPlayLists(MusicFan user, Map<String, Object> context) {
		LOGGER.info("Finding shared playlists");
		List<PlayList> availablepl = PersistenceService
				.findAvailablePlayListFor(user);
		LOGGER.info("Found " + availablepl.toString());
		if (availablepl.isEmpty()) {
			LOGGER.warning("No propositions ???");
		} else {
			context.put("proposals", availablepl);
			LOGGER.info("Added proposals " + availablepl);
		}
	}
}
