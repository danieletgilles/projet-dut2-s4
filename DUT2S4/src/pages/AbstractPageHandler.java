package pages;

import static common.AppConstants.PLAYLIST;
import static common.AppConstants.USER;
import static common.AppConstants.USER_COOKIE;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Map;

import org.simpleframework.http.Cookie;
import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import common.Handler;
import common.SessionManager;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public abstract class AbstractPageHandler implements Handler {
	protected void clearSession(Request request, Response response) {
		Cookie cookie = request.getCookie(USER_COOKIE);
		if (cookie != null) {
			cookie.setExpiry(0);
			response.setCookie(cookie);
		}
		SessionManager.getSession(request, response).removeAttribute(USER);
		SessionManager.getSession(request, response).removeAttribute(PLAYLIST);
	}

	protected String handle(Configuration cfg, Map<String, Object> context,
			Request request, Response response, String templateName) {
		try {
			Writer body = new OutputStreamWriter(response.getPrintStream(),
					Charset.forName("UTF-8"));
			long time = System.currentTimeMillis();
			Template temp = cfg.getTemplate(templateName);
			temp.process(context, body);
			response.setValue("Content-Type", "text/html");
			response.setValue("Server", "HelloWorld/1.0 (Simple 4.0)");
			response.setDate("Date", time);
			response.setDate("Last-Modified", time);
			body.close();
		} catch (TemplateException e) {
			LOGGER.severe(e.getMessage());
		} catch (IOException e) {
			LOGGER.severe(e.getMessage());
		}
		return DONE;
	}

}
