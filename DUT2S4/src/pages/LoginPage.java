package pages;

import java.util.Map;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import freemarker.template.Configuration;

public class LoginPage extends AbstractPageHandler {

	@Override
	public String handle(Configuration cfg, Map<String, Object> context,
			Request request, Response response) {
		clearSession(request, response);
		handle(cfg, context, request, response, "login.ftl");
		return DONE;
	}
}
