package httpserver;

import static common.AppConstants.ARTWORK_ACTION;
import static common.AppConstants.FILTER_ACTION;
import static common.AppConstants.FOLLOW_ACTION;
import static common.AppConstants.HOME_PAGE;
import static common.AppConstants.KEYWORD_ACTION;
import static common.AppConstants.LOGIN_ACTION;
import static common.AppConstants.LOGIN_PAGE;
import static common.AppConstants.MUSICDIR_FILE;
import static common.AppConstants.PLAYLIST_ACTION;
import static common.AppConstants.PLAYLIST_MANAGEMENT_ACTION;
import static common.AppConstants.PLAY_ACTION;
import static common.AppConstants.REGISTER_ACTION;
import static common.AppConstants.REGISTER_PAGE;
import static common.AppConstants.SERVER_PORT;
import static common.AppConstants.SHARE_ACTION;
import static common.AppConstants.SONG_ACTION;
import static common.AppConstants.UPLOAD_ACTION;
import static common.AppConstants.VOTE_ACTION;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import model.MusicFan;

import org.simpleframework.http.Path;
import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.simpleframework.http.core.Container;
import org.simpleframework.http.core.ContainerServer;
import org.simpleframework.transport.Server;
import org.simpleframework.transport.connect.Connection;
import org.simpleframework.transport.connect.SocketConnection;

import pages.HomePage;
import pages.LoginPage;
import pages.RegisterPage;
import persistence.PersistenceService;
import actions.ArtWork;
import actions.FollowerAction;
import actions.LoginAction;
import actions.MotsClesAction;
import actions.PlayAction;
import actions.PlayListAction;
import actions.PlayListManagementAction;
import actions.RegisterAction;
import actions.ShareAction;
import actions.SongAction;
import actions.SongByKeyworkAction;
import actions.UploadAction;
import actions.VoteAction;

import common.Handler;
import common.MyUtils;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;

public class HttpServer implements Container {

	private static final Logger LOGGER = Logger.getLogger("dut2s4.httpserver");
	private Configuration cfg = new Configuration();
	private Map<String, Handler> handlers = new HashMap<String, Handler>();
	private Map<String, String> staticContent = new HashMap<String, String>();
	private LoginAction loginHandler = new LoginAction();

	public HttpServer() {
		cfg.setTemplateLoader(new ClassTemplateLoader(HttpServer.class,
				"/templates"));
		cfg.setObjectWrapper(new DefaultObjectWrapper());
		handlers.put(HOME_PAGE, new HomePage());
		handlers.put(LOGIN_PAGE, new LoginPage());
		handlers.put(REGISTER_PAGE, new RegisterPage());
		handlers.put(UPLOAD_ACTION, new UploadAction());
		handlers.put(KEYWORD_ACTION, new MotsClesAction());
		handlers.put(ARTWORK_ACTION, new ArtWork());
		handlers.put(PLAY_ACTION, new PlayAction());
		handlers.put(SONG_ACTION, new SongAction());
		handlers.put(PLAYLIST_ACTION, new PlayListAction());
		handlers.put(VOTE_ACTION, new VoteAction());
		handlers.put(SHARE_ACTION, new ShareAction());
		handlers.put(FOLLOW_ACTION, new FollowerAction());
		handlers.put(PLAYLIST_MANAGEMENT_ACTION, new PlayListManagementAction());
		handlers.put(FILTER_ACTION, new SongByKeyworkAction());
		handlers.put(LOGIN_ACTION, loginHandler);
		handlers.put(REGISTER_ACTION, new RegisterAction());
		staticContent.put("css", "text/css");
		staticContent.put("js", "text/javascript");
		staticContent.put("png", "image/png");
		staticContent.put("jpg", "image/jpeg");
		staticContent.put("gif", "image/gif");
	}

	public void handle(Request request, Response response) {
		Path path = request.getPath();
		LOGGER.info("Handling " + path);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String extension = path.getExtension();
			String mimeType = staticContent.get(extension);
			if (mimeType == null) {
				String[] command = path.getSegments();
				String page;
				if (command.length > 0) {
					page = command[0];
				} else {
					page = HOME_PAGE;
				}
				boolean connected = loginHandler.fillInContext(map, request,
						response);
				Handler handler;

				if (!connected && !LOGIN_ACTION.equals(page)
						&& !REGISTER_PAGE.equals(page)
						&& !REGISTER_ACTION.equals(page)) {
					page = LOGIN_PAGE;
				}
				while (page != Handler.DONE) {
					handler = handlers.get(page);
					if (handler == null) {
						page = HOME_PAGE;
					} else {
						page = handler.handle(cfg, map, request, response);
					}
				}
			} else {
				serveStaticFile(mimeType, path.toString(), response);
			}
		} catch (Exception e) {
			LOGGER.severe(e.toString());
		}
	}

	private void serveStaticFile(String mimeType, String filename,
			Response response) throws IOException {
		response.setValue("Content-Type", mimeType);
		LOGGER.info("Serving static file " + filename);
		InputStream in = HttpServer.class.getResource(filename).openStream();
		OutputStream out = response.getPrintStream();
		MyUtils.copy(in, out);
		in.close();
		out.close();
	}

	public static void main(String[] args) throws IOException {
		if (!MUSICDIR_FILE.exists()) {
			LOGGER.info("Initializing Music directory and DB");
			MusicFan daniel = new MusicFan("Daniel Le Berre", "zzz",
					"leberre@cril.fr");
			daniel.setUsername("leberre");

			MusicFan gilles = new MusicFan("Gilles Audemard", "yyy",
					"audemard@cril.fr");
			gilles.setUsername("audemard");
			MusicFan stephanie = new MusicFan("Stéphanie Roussel", "xxx",
					"stephanie.roussel@cril.fr");
			stephanie.setUsername("sroussel");
			PersistenceService.create(daniel);
			PersistenceService.create(gilles);
			PersistenceService.create(stephanie);
		}
		LOGGER.info("Starting server");
		Container container = new HttpServer();
		Server server = new ContainerServer(container);
		Connection connection = new SocketConnection(server);
		SocketAddress address = new InetSocketAddress(SERVER_PORT);
		connection.connect(address);
		LOGGER.info("Server started");
	}
}
