package model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;

@Entity
@IdClass(value = VotePK.class)
public class Vote {

	private int score;

	@Id
	@ManyToOne
	private MusicFan voter;

	@Id
	@ManyToOne
	private Song song;

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public MusicFan getVoter() {
		return voter;
	}

	public void setVoter(MusicFan voter) {
		this.voter = voter;
	}

	public Song getSong() {
		return song;
	}

	public void setSong(Song song) {
		this.song = song;
	}
}
