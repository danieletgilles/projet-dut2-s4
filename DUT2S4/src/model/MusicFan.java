package model;

import static common.AppConstants.UTF8;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class MusicFan {

	@Id
	@GeneratedValue
	private long id;

	private String fullname;
	private String password;
	private String email;
	@Column(unique = true)
	private String username;

	@OneToMany
	private List<Song> songs = new ArrayList<Song>();

	@OneToMany(mappedBy = "owner")
	private List<PlayList> playlists = new ArrayList<PlayList>();

	@ManyToMany(mappedBy = "followers")
	private List<PlayList> follows = new ArrayList<PlayList>();

	@OneToMany(mappedBy = "voter")
	private List<Vote> votes = new ArrayList<Vote>();

	private static MessageDigest md5;

	static {
		try {
			md5 = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			md5 = null;
		}
	}

	public MusicFan() {

	}

	public MusicFan(String name, String password, String email) {
		this.fullname = name;
		this.email = email;
		if (md5 == null) {
			this.password = password;
		} else {
			md5.update(password.getBytes(UTF8));
			this.password = new String(md5.digest(), UTF8);
		}
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String name) {
		this.fullname = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<PlayList> getPlaylists() {
		return playlists;
	}

	public void setPlaylists(List<PlayList> playlists) {
		this.playlists = playlists;
	}

	public List<PlayList> getFollows() {
		return follows;
	}

	public void setFollows(List<PlayList> follows) {
		this.follows = follows;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Vote getVoteFor(Song s) {
		for (Vote vote : votes) {
			if (vote.getSong() == s) {
				return vote;
			}
		}
		return null;
	}

	public List<Vote> getVotes() {
		return votes;
	}

	public void setVotes(List<Vote> votes) {
		this.votes = votes;
	}

	@Override
	public String toString() {
		return fullname;
	}
}
