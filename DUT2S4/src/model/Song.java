package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

@Entity
public class Song {

	@Id
	@GeneratedValue
	private long id;

	private String title;

	private String artist;

	private String album;

	@Column(name = "APPROXYEAR")
	private String year;

	private int length;

	private String location;

	private boolean hasArtwork;

	@ManyToMany
	@OrderBy("name")
	private List<Label> labels = new ArrayList<Label>();

	@OneToMany(mappedBy = "song")
	private List<Vote> votes = new ArrayList<Vote>();

	public Song() {

	}

	public Song(String title, int length) {
		this.title = title;
		this.length = length;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public List<Label> getLabels() {
		return labels;
	}

	public void setLabels(List<Label> labels) {
		this.labels = labels;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public boolean isHasArtwork() {
		return hasArtwork;
	}

	public void setHasArtwork(boolean hasArtwork) {
		this.hasArtwork = hasArtwork;
	}

	public List<Vote> getVotes() {
		return votes;
	}

	public void setVotes(List<Vote> votes) {
		this.votes = votes;
	}

	public String getExt() {
		return location.substring(location.length() - 3, location.length());
	}

	@Override
	public String toString() {
		return title + " (" + artist + ")";
	}
}
