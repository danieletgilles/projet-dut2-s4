package model;

public class VotePK {

	private long voter;

	private long song;

	public long getVoter() {
		return voter;
	}

	public void setVoter(long voter) {
		this.voter = voter;
	}

	public long getSong() {
		return song;
	}

	public void setSong(long song) {
		this.song = song;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (song ^ (song >>> 32));
		result = prime * result + (int) (voter ^ (voter >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		VotePK other = (VotePK) obj;
		if (song != other.song) {
			return false;
		}
		if (voter != other.voter) {
			return false;
		}
		return true;
	}
}
