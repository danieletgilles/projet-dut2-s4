package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class PlayList {

	@Id
	@GeneratedValue
	private long id;

	private String name;

	private boolean shared;

	@ManyToOne
	private MusicFan owner;

	@OneToMany(fetch = FetchType.EAGER)
	private List<Song> songs = new ArrayList<Song>();

	@ManyToMany
	private List<MusicFan> followers = new ArrayList<MusicFan>();

	public PlayList() {

	}

	public PlayList(String name, MusicFan owner) {
		this.name = name;
		this.owner = owner;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MusicFan getOwner() {
		return owner;
	}

	public void setOwner(MusicFan owner) {
		this.owner = owner;
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}

	public List<MusicFan> getFollowers() {
		return followers;
	}

	public void setFollowers(List<MusicFan> followers) {
		this.followers = followers;
	}

	public boolean isShared() {
		return shared;
	}

	public void setShared(boolean shared) {
		this.shared = shared;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Playlist " + name;
	}
}
