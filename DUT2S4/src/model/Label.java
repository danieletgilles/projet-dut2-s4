package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Label {

	@Id
	@GeneratedValue
	private long id;

	@Column(unique = true)
	private String name;

	@ManyToMany(mappedBy = "labels")
	private List<Song> songs = new ArrayList<Song>();

	public Label() {
	}

	public Label(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}

	public String toString() {
		return name;
	}
}
