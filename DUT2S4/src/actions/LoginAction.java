package actions;

import static common.AppConstants.ERROR;
import static common.AppConstants.LOGIN_PAGE;
import static common.AppConstants.USER;
import static common.AppConstants.USER_COOKIE;
import static common.AppConstants.UTF8;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import model.MusicFan;

import org.simpleframework.http.Cookie;
import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import persistence.PersistenceService;

import common.Session;
import common.SessionManager;

import freemarker.template.Configuration;

public class LoginAction extends AbstractHandler {

	private static MessageDigest md5;

	static {
		try {
			md5 = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			md5 = null;
		}
	}

	public LoginAction() {

	}

	@Override
	public String handle(Configuration cfg, Map<String, Object> context,
			Request request, Response response) {
		String username = request.getParameter("username");
		if (username == null || username.isEmpty()) {
			context.put(ERROR, "l'identifiant est obligatoire");
			return LOGIN_PAGE;
		}
		String passwd = request.getParameter("password");
		if (passwd == null || passwd.isEmpty()) {
			context.put(ERROR, "le mot de passe est obligatoire");
			return LOGIN_PAGE;
		}
		String digest;
		if (md5 == null) {
			digest = passwd;
		} else {
			md5.update(passwd.getBytes(UTF8));
			digest = new String(md5.digest(), UTF8);
		}
		MusicFan user = PersistenceService.findUserByUsername(username);
		if (user != null && digest.equals(user.getPassword())) {
			SessionManager.getSession(request, response).setAttribute(USER,
					user);
			if ("remember-me".equals(request.getParameter("remember"))) {
				response.setCookie(USER_COOKIE, username);
			}
			return redirect(response, "/");
		}
		context.put(ERROR, "l'identifiant ou le mot de passe sont incorrects");
		return LOGIN_PAGE;
	}

	/**
	 * Session management. Checks if a user is logged, and fill in the context
	 * appropriately.
	 * 
	 * @param context
	 * @param request
	 * @return true iff someone is identified.
	 */
	public boolean fillInContext(Map<String, Object> context, Request request,
			Response response) {
		Session session = SessionManager.getSession(request, response);
		for (Map.Entry<String, Object> entry : session.attributes()) {
			context.put(entry.getKey(), entry.getValue());
		}
		LOGGER.info(context.toString());
		if (session.getAttribute(USER) != null) {
			return true;
		}
		Cookie cookie = request.getCookie(USER_COOKIE);
		if (cookie == null) {
			return false;
		}
		MusicFan mf = PersistenceService.findUserByUsername(cookie.getValue());
		if (mf == null) {
			return false;
		}
		session.setAttribute(USER, mf);
		context.put(USER, mf);
		return true;
	}
}
