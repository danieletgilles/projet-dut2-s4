package actions;

import static common.AppConstants.FOLLOW_ACTION;
import static common.AppConstants.USER;

import java.util.Map;

import model.MusicFan;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import persistence.PersistenceService;

import common.SessionManager;

import freemarker.template.Configuration;

public class FollowerAction extends AbstractHandler {

	private static final int PLAYLIST_ID = 2;
	private static final int COMMAND = 1;
	private static final int ROOT = 0;

	@Override
	public String handle(Configuration cfg, Map<String, Object> context,
			Request request, Response response) {
		String[] segments = request.getPath().getSegments();
		MusicFan mf = (MusicFan) SessionManager.getSession(request, response)
				.getAttribute(USER);
		if (segments.length == 3) {
			assert FOLLOW_ACTION.equals(segments[ROOT]);
			long plid = Long.valueOf(segments[PLAYLIST_ID]);
			boolean follows = "follow".equals(segments[COMMAND]);

			boolean updated = PersistenceService.follow(mf, plid, follows);
			if (updated) {
				LOGGER.info("Playlist " + plid
						+ (follows ? " followed" : " discarded") + " for user "
						+ mf.getFullname());
			} else {
				LOGGER.info("Cannot follow playlist " + plid + " for user "
						+ mf.getFullname());
			}
		}
		return redirect(response, "/");
	}

}
