package actions;

import static common.AppConstants.ERROR;
import static common.AppConstants.REGISTER_PAGE;
import static common.AppConstants.USER;

import java.util.Map;

import model.MusicFan;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import persistence.PersistenceService;

import common.SessionManager;

import freemarker.template.Configuration;

public class RegisterAction extends AbstractHandler {

	@Override
	public String handle(Configuration cfg, Map<String, Object> context,
			Request request, Response response) {
		String username = request.getParameter("username");
		if (username == null || username.isEmpty()) {
			context.put(ERROR, "l'identifiant est obligatoire");
			return REGISTER_PAGE;
		}
		MusicFan user = PersistenceService.findUserByUsername(username);
		if (user != null) {
			context.put(ERROR, "cet identifiant est déjà utilisé");
			return REGISTER_PAGE;
		}
		String fullname = request.getParameter("fullname");
		if (fullname == null || fullname.isEmpty()) {
			context.put(ERROR, "le nom complet est obligatoire");
			return REGISTER_PAGE;
		}
		String passwd = request.getParameter("password");
		if (passwd == null || passwd.isEmpty()) {
			context.put(ERROR, "le mot de passe est obligatoire");
			return REGISTER_PAGE;
		}
		String email = request.getParameter("email");
		if (email == null || email.isEmpty()) {
			context.put(ERROR, "l'email est obligatoire");
			return REGISTER_PAGE;
		}
		user = new MusicFan(fullname, passwd, email);
		user.setUsername(username);
		PersistenceService.create(user);
		SessionManager.getSession(request, response).setAttribute(USER, user);
		return redirect(response, "/");
	}
}
