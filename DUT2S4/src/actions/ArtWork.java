package actions;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import model.Song;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagField;
import org.jaudiotagger.tag.images.Artwork;
import org.jaudiotagger.tag.mp4.Mp4FieldKey;
import org.jaudiotagger.tag.mp4.Mp4Tag;
import org.simpleframework.http.Path;
import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import persistence.PersistenceService;

import common.Handler;

import freemarker.template.Configuration;

public class ArtWork implements Handler {

	@Override
	public String handle(Configuration cfg, Map<String, Object> context,
			Request request, Response response) {
		Path path = request.getPath();
		Long songId = Long.valueOf(path.getSegments()[1]);
		Song song = PersistenceService.findSongById(songId);
		String file = song.getLocation();
		LOGGER.info("Retrieving artwork for " + file);
		AudioFile f;
		try {
			f = AudioFileIO.read(new File(file));
			Tag tag = f.getTag();
			List<Artwork> artworks = tag.getArtworkList();
			if (artworks.isEmpty()) {
				if (file.endsWith(".m4u")) {
					Mp4Tag m4tag = (Mp4Tag) tag;
					TagField covrArtField = m4tag
							.getFirstField(Mp4FieldKey.ARTWORK);
					if (covrArtField != null) {
						response.getPrintStream().write(
								covrArtField.getRawContent());
					} else {
						LOGGER.info("Sorry, the cover art is empty");
					}
				} else {
					LOGGER.info("Sorry, the artwork is empty");
				}
			} else {
				Artwork artwork = artworks.get(0);
				response.setValue("Content-Type", artwork.getMimeType());
				response.getPrintStream().write(artwork.getBinaryData());
				LOGGER.info(artwork.getDescription() + " "
						+ artwork.getMimeType());
			}
		} catch (Exception e) {
			LOGGER.warning(e.toString());
		} finally {
			try {
				response.close();
			} catch (IOException e) {
				LOGGER.warning(e.toString());
			}
		}
		return DONE;
	}

}
