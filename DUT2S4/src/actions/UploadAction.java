package actions;

import static common.AppConstants.ERROR;
import static common.AppConstants.HOME_PAGE;
import static common.AppConstants.MUSICDIR_FILE;
import static common.AppConstants.PLAYLIST;
import static common.AppConstants.USER;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import model.MusicFan;
import model.PlayList;
import model.Song;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.AudioHeader;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagException;
import org.jaudiotagger.tag.TagField;
import org.jaudiotagger.tag.mp4.Mp4FieldKey;
import org.jaudiotagger.tag.mp4.Mp4Tag;
import org.simpleframework.http.Part;
import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import persistence.PersistenceService;

import common.MyUtils;
import common.Session;
import common.SessionManager;

import freemarker.template.Configuration;

public class UploadAction extends AbstractHandler {
	private static final Logger LOGGER = Logger.getLogger("dut2s4.httpserver");
	private static List<String> authorizedExtensions = new ArrayList<String>();
	static {
		authorizedExtensions.add("mp3");
		authorizedExtensions.add("ogg");
		authorizedExtensions.add("m4a");
		authorizedExtensions.add("m4p");
	}

	public UploadAction() {
		if (!MUSICDIR_FILE.exists()) {
			boolean created = MUSICDIR_FILE.mkdir();
			if (!created) {
				LOGGER.severe("Cannot create directory "
						+ MUSICDIR_FILE.getAbsolutePath());
			}
		}
	}

	@Override
	public String handle(Configuration cfg, Map<String, Object> context,
			Request request, Response response) {
		Session session = SessionManager.getSession(request, response);
		MusicFan user = session.getAttribute(USER);
		PlayList playlist = session.getAttribute(PLAYLIST);
		if (playlist == null) {
			playlist = user.getPlaylists().get(0);
			session.setAttribute(PLAYLIST, playlist);
		}
		assert user != null;
		File userDir = new File(MUSICDIR_FILE.getAbsolutePath()
				+ File.separator + user.getEmail().replace("@", "_"));
		if (!userDir.exists()) {
			boolean created = userDir.mkdir();
			if (!created) {
				LOGGER.severe("Cannot create user dir "
						+ userDir.getAbsolutePath());
			}
		}
		LOGGER.info("Uploading file for user ");
		for (Part part : request.getParts()) {
			LOGGER.info("reçu fichier " + part.getFileName());
			String filename = part.getFileName();
			String extension = filename
					.substring(filename.lastIndexOf('.') + 1);
			if (!authorizedExtensions.contains(extension)) {
				context.put(ERROR,
						"Seul les fichiers mp3, m4a et ogg vorbis sont supportés !");
			} else {
				File saved = new File(userDir.getAbsolutePath()
						+ File.separator + part.getFileName());
				try {
					InputStream in = part.getInputStream();
					FileOutputStream out = new FileOutputStream(saved);
					MyUtils.copy(in, out);
					in.close();
					out.close();
					Song song = extractMp3Infos(saved);
					PersistenceService.persist(song, user, playlist);
					playlist.getSongs().add(song);
					user.getSongs().add(song);
					LOGGER.warning(song.toString());
				} catch (Exception e) {
					LOGGER.severe(e.getMessage());
				}
			}
		}
		if (context.get(ERROR) == null) {
			return redirect(response, "/");
		}
		return HOME_PAGE;
	}

	public static Song extractMp3Infos(File file) throws CannotReadException,
			IOException, TagException, ReadOnlyFileException,
			InvalidAudioFrameException {
		AudioFile f = AudioFileIO.read(file);
		AudioHeader header = f.getAudioHeader();
		Song song = new Song();
		if (header != null) {
			song.setLength(header.getTrackLength());
		}
		song.setLocation(file.getAbsolutePath());
		Tag tag = f.getTag();
		if (tag != null) {
			song.setTitle(tag.getFirst(FieldKey.TITLE));
			song.setArtist(tag.getFirst(FieldKey.ARTIST));
			song.setAlbum(tag.getFirst(FieldKey.ALBUM));
			song.setYear(tag.getFirst(FieldKey.YEAR));
			song.setHasArtwork(!tag.getArtworkList().isEmpty());
			if (file.getName().endsWith(".m4a")) {
				Mp4Tag m4tag = (Mp4Tag) tag;
				TagField covrArtField = m4tag
						.getFirstField(Mp4FieldKey.ARTWORK);
				if (covrArtField != null) {
					song.setHasArtwork(!covrArtField.isEmpty());
				}
			}
		}
		return song;
	}

}
