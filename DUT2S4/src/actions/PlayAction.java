package actions;

import static common.AppConstants.A_LECOUTE;
import static common.AppConstants.ERROR;
import static common.AppConstants.HOME_PAGE;

import java.util.Map;

import model.Song;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import persistence.PersistenceService;

import common.Session;
import common.SessionManager;

import freemarker.template.Configuration;

public class PlayAction extends AbstractHandler {

	private static final int TRACK_ID = 1;

	@Override
	public String handle(Configuration cfg, Map<String, Object> context,
			Request request, Response response) {
		String songid = request.getPath().getSegments()[TRACK_ID];
		Session session = SessionManager.getSession(request, response);
		if (songid != null) {
			try {
				long id = Long.valueOf(songid);
				Song song = PersistenceService.findSongById(id);
				session.setAttribute(A_LECOUTE, song);
				return redirect(response, "/");
			} catch (NumberFormatException nfe) {
				context.put(ERROR, "Mauvais identifiant de morceau : " + songid);
			}
		} else {
			context.put(ERROR, "Pas d'identifiant de morceau");
		}
		return HOME_PAGE;
	}

}
