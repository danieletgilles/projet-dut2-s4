package actions;

import java.io.IOException;

import org.simpleframework.http.Response;

import common.Handler;


public abstract class AbstractHandler implements Handler {

	private static final int REDIRECT_HTTP_RESPONSE_CODE = 303;

	public String redirect(Response response, String location) {
		response.setCode(REDIRECT_HTTP_RESPONSE_CODE);
		response.setValue("Location", location);
		try {
			response.close();
		} catch (IOException e) {
			LOGGER.severe(e.getMessage());
		}
		return DONE;
	}

}
