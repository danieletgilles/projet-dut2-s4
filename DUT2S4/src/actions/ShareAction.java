package actions;

import static common.AppConstants.SHARE_ACTION;
import static common.AppConstants.USER;

import java.util.Map;

import model.MusicFan;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import persistence.PersistenceService;

import common.SessionManager;

import freemarker.template.Configuration;

public class ShareAction extends AbstractHandler {

	private static final int PLAYLIST_ID = 2;
	private static final int COMMAND = 1;
	private static final int ROOT = 0;

	@Override
	public String handle(Configuration cfg, Map<String, Object> context,
			Request request, Response response) {
		String[] segments = request.getPath().getSegments();
		MusicFan mf = (MusicFan) SessionManager.getSession(request, response)
				.getAttribute(USER);
		if (segments.length == 3) {
			assert SHARE_ACTION.equals(segments[ROOT]);
			long plid = Long.valueOf(segments[PLAYLIST_ID]);
			boolean shares = "share".equals(segments[COMMAND]);
			boolean updated = PersistenceService.share(mf, plid, shares);
			if (updated) {
				LOGGER.info("Playlist " + plid
						+ (shares ? " shared " : " hidden ") + " for user "
						+ mf.getFullname());
			} else {
				LOGGER.info("Cannot share playlist " + plid + " for user "
						+ mf.getFullname());
			}
		}
		return redirect(response, "/");
	}

}
