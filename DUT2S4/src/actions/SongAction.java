package actions;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import model.Song;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import persistence.PersistenceService;

import common.Handler;
import common.MyUtils;

import freemarker.template.Configuration;

public class SongAction implements Handler {

	private Map<String, String> musicContent = new HashMap<String, String>();

	public SongAction() {
		musicContent.put("mp3", "audio/mpeg");
		musicContent.put("ogg", "audio/ogg");
		musicContent.put("m4a", "audio/mp4");
	}

	@Override
	public String handle(Configuration cfg, Map<String, Object> context,
			Request request, Response response) {
		try {
			String[] pieces = request.getPath().getSegments()[1].split("\\.");
			long songId = Long.valueOf(pieces[0]);
			LOGGER.info("Serving song " + songId);
			Song song = PersistenceService.findSongById(songId);
			String extension = pieces[1];
			LOGGER.info("With extension " + extension);
			serveDynamicFile(musicContent.get(extension), song.getLocation(),
					response);
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
		}
		return DONE;
	}

	private void serveDynamicFile(String mimeType, String filename,
			Response response) throws IOException {
		response.setValue("Content-Type", mimeType);
		LOGGER.info("Serving dynamic file " + filename);
		long begin = System.currentTimeMillis();
		InputStream in = new FileInputStream(filename);
		try {
			OutputStream out = response.getPrintStream();
			try {
				MyUtils.copy(in, out);
			} finally {
				out.close();
			}
		} finally {
			in.close();
		}
		long end = System.currentTimeMillis();
		LOGGER.info("Done " + (end - begin) / 1000 + " s");
	}
}
