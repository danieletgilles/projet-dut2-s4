package actions;

import static common.AppConstants.*;

import java.util.Map;

import model.PlayList;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import persistence.PersistenceService;

import common.AppConstants;
import common.Session;
import common.SessionManager;

import freemarker.template.Configuration;

public class PlayListAction extends AbstractHandler {

	private static final int SONG_INDEX_IN_PLAYLIST = 3;
	private static final int PLAYLIST_ID = 2;
	private static final int ACTION = 1;
	private static final String STOP = "stop";

	@Override
	public String handle(Configuration cfg, Map<String, Object> context,
			Request request, Response response) {
		// /playlist/action/playlistid/songindex
		String[] segments = request.getPath().getSegments();
		Session session = SessionManager.getSession(request, response);
		if (segments.length <= PLAYLIST_ID) {
			context.put(ERROR, "mauvaise commande de playlist");
			return HOME_PAGE;
		}
		String action = segments[ACTION];
		long playlistid;
		try {
			playlistid = Long.valueOf(segments[PLAYLIST_ID]);
		} catch (NumberFormatException nfe) {
			context.put(ERROR, "Mauvais identifiant de playlist : "
					+ segments[PLAYLIST_ID]);
			return HOME_PAGE;
		}
		PlayList pl = PersistenceService.findPlayListById(playlistid);
		if (pl == null) {
			context.put(ERROR, "cette playlist n'existe pas/plus " + playlistid);
			return HOME_PAGE;
		}
		int index = 0;
		if (segments.length == 4) {
			try {
				index = Integer.valueOf(segments[SONG_INDEX_IN_PLAYLIST]);
			} catch (NumberFormatException ex) {
				context.put(ERROR, "indice de playlist incorrect : "
						+ segments[3]);
				return HOME_PAGE;
			}
		}
		if ("next".equals(action)) {
			index++;
			if (index == pl.getSongs().size()) {
				action = STOP;
			}
		} else if ("prev".equals(action)) {
			if (index > 0) {
				index--;
			} else {
				action = STOP;
			}
		} else if ("add".equals(action)) {
			PersistenceService.addSongToPlayList(pl, index);
			context.put(MESSAGE, "Morceau ajouté à la playlist");
			action = STOP;
		} else if ("remove".equals(action)) {
			PersistenceService.removeSongFromPlayList(pl, index);
			context.put(MESSAGE, "Morceau enlevé de la playlist");
			action = STOP;
		}
		if (STOP.equals(action)) {
			session.removeAttribute(A_LECOUTE);
			session.removeAttribute(AppConstants.PLAYLIST_ID);
		} else {
			session.setAttribute(A_LECOUTE, pl.getSongs().get(index));
			session.setAttribute(AppConstants.PLAYLIST_ID, index);
		}
		session.setAttribute(PLAYLIST, pl);
		return redirect(response, "/");
	}
}
