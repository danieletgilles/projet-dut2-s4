package actions;

import static common.AppConstants.ERROR;
import static common.AppConstants.HOME_PAGE;
import static common.AppConstants.PLAYLIST;

import java.util.Arrays;
import java.util.Map;

import model.PlayList;
import model.Song;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import persistence.PersistenceService;

import common.SessionManager;

import freemarker.template.Configuration;

public class MotsClesAction extends AbstractHandler {

	private static final int SONG_INDEX = 1;

	@Override
	public String handle(Configuration cfg, Map<String, Object> context,
			Request request, Response response) {
		String motscles = request.getParameter("motscles");
		String trackIndex = request.getPath().getSegments()[SONG_INDEX];
		if (trackIndex != null) {
			try {
				int index = Integer.valueOf(trackIndex);
				PlayList playlist = SessionManager
						.getSession(request, response).getAttribute(PLAYLIST);
				Song song = playlist.getSongs().get(index);
				String[] mots = motscles.split(",");
				LOGGER.warning(Arrays.asList(mots).toString());
				PersistenceService.setKeywords(song, mots);
				return redirect(response, "/");
			} catch (NumberFormatException nfe) {
				context.put(ERROR, "Mauvais numéro de piste : " + trackIndex);
			}
		} else {
			context.put(ERROR, "Pas de numéro de piste");
		}
		return HOME_PAGE;
	}
}
