package actions;

import static common.AppConstants.A_LECOUTE;
import static common.AppConstants.ERROR;
import static common.AppConstants.HOME_PAGE;
import static common.AppConstants.PLAYLIST;
import static common.AppConstants.PLAYLIST_ID;
import static common.AppConstants.USER;

import java.util.Map;

import model.Label;
import model.MusicFan;
import model.PlayList;
import model.Song;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import common.Session;
import common.SessionManager;

import freemarker.template.Configuration;

public class SongByKeyworkAction extends AbstractHandler {

	private static final int LABEL_INDEX = 2;
	private static final int SONG_INDEX = 1;

	@Override
	public String handle(Configuration cfg, Map<String, Object> context,
			Request request, Response response) {
		String[] segments = request.getPath().getSegments();
		if (segments.length == 3) {
			try {
				int songIndex = Integer.valueOf(segments[SONG_INDEX]);
				int labelIndex = Integer.valueOf(segments[LABEL_INDEX]);
				Session session = SessionManager.getSession(request, response);
				PlayList currentpl = session.getAttribute(PLAYLIST);
				if (currentpl.getSongs().size() <= songIndex) {
					context.put(ERROR,
							"mauvais index de morceau dans la playlist :"
									+ songIndex);
					return HOME_PAGE;
				}
				Label label = currentpl.getSongs().get(songIndex).getLabels()
						.get(labelIndex);
				PlayList pl = new PlayList();
				MusicFan user = session.getAttribute(USER);
				pl.setOwner(user);
				pl.setName("Avec mot clé " + label.getName());
				for (Song song : label.getSongs()) {
					pl.getSongs().add(song);
				}
				session.setAttribute(PLAYLIST, pl);
				session.removeAttribute(A_LECOUTE);
				session.removeAttribute(PLAYLIST_ID);
				return redirect(response, "/");
			} catch (NumberFormatException nfe) {
				context.put(ERROR, "mauvais index " + nfe.getMessage());
			}
		}
		return HOME_PAGE;
	}
}
