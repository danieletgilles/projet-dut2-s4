package actions;

import static common.AppConstants.A_LECOUTE;
import static common.AppConstants.PLAYLIST;
import static common.AppConstants.PLAYLIST_ID;
import static common.AppConstants.USER;

import java.util.Map;

import model.MusicFan;
import model.PlayList;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import persistence.PersistenceService;

import common.Session;
import common.SessionManager;

import freemarker.template.Configuration;

public class PlayListManagementAction extends AbstractHandler {

	private static final int PLAYLIST_ORIGIN = 3;
	private static final int PLAYLIST_INDEX = 2;

	@Override
	public String handle(Configuration cfg, Map<String, Object> context,
			Request request, Response response) {
		Session session = SessionManager.getSession(request, response);
		MusicFan user = session.getAttribute(USER);
		String[] segments = request.getPath().getSegments();
		if (segments.length > 1) {
			assert "managelist".equals(segments[1]);
			if ("create".equals(segments[1])) {
				String title = request.getParameter("title");
				PlayList pl = new PlayList();
				pl.setName(title);
				pl.setOwner(user);
				user.getPlaylists().add(pl);
				PersistenceService.create(pl);
				session.setAttribute(PLAYLIST, pl);
			} else if ("change".equals(segments[1])) {
				int playlistindex = Integer.valueOf(segments[PLAYLIST_INDEX]);
				PlayList pl;
				if ("mine".equals(segments[PLAYLIST_ORIGIN])) {
					pl = user.getPlaylists().get(playlistindex);
				} else if ("followed".equals(segments[PLAYLIST_ORIGIN])) {
					pl = user.getFollows().get(playlistindex);
				} else {
					pl = PersistenceService.findPlayListForUser(user,
							playlistindex);
				}
				if (pl != null) {
					session.setAttribute(PLAYLIST, pl);
					session.removeAttribute(A_LECOUTE);
					session.removeAttribute(PLAYLIST_ID);
				}
			}
		}
		return redirect(response, "/");
	}
}
