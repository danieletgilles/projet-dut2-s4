package actions;

import static common.AppConstants.ERROR;
import static common.AppConstants.HOME_PAGE;
import static common.AppConstants.PLAYLIST;
import static common.AppConstants.USER;

import java.util.Map;

import model.MusicFan;
import model.PlayList;
import model.Song;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import persistence.PersistenceService;

import common.Session;
import common.SessionManager;

import freemarker.template.Configuration;

public class VoteAction extends AbstractHandler {

	private static final int SONG_INDEX = 1;

	@Override
	public String handle(Configuration cfg, Map<String, Object> context,
			Request request, Response response) {
		Session session = SessionManager.getSession(request, response);
		MusicFan user = session.getAttribute(USER);
		PlayList playlist = session.getAttribute(PLAYLIST);
		try {
			int songIndex = Integer.valueOf(request.getPath().getSegments()[SONG_INDEX]);
			Song song = playlist.getSongs().get(songIndex);
			int score = Integer.valueOf(request.getParameter("score"));
			if (score <= 5 && score >= 0) {
				LOGGER.info("Voting for " + song.getTitle() + " with score "
						+ score);
				PersistenceService.registerVote(user, song, score);
				return redirect(response, "/");
			} else {
				context.put(ERROR, "Valeur invalide pour le score : " + score);
			}
		} catch (Exception ex) {
			context.put(ERROR, "Problème lors du vote : " + ex.getMessage());
		}
		return HOME_PAGE;
	}
}
