package persistence;

import static org.eclipse.persistence.config.PersistenceUnitProperties.JDBC_URL;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import model.Label;
import model.MusicFan;
import model.PlayList;
import model.Song;
import model.Vote;

import common.AppConstants;

public final class PersistenceService {

	private static EntityManagerFactory factory;
	private static EntityManager em;

	static {
		Map<String, String> properties = new HashMap<String, String>();
		properties
				.put(JDBC_URL,
						"jdbc:derby:"
								+ AppConstants.MUSICDIR_FILE.getAbsolutePath()
								+ File.separator + AppConstants.DBNAME
								+ ";create=true");
		factory = Persistence.createEntityManagerFactory("dut2music",
				properties);
		// creating only one instance of the entity manager
		// to ensure identity between objects representing the same
		// data in the database.
		em = factory.createEntityManager();
	}

	private PersistenceService() {
		// to avoid that instances of that class are created
	}

	public static void create(MusicFan user) {
		user.getPlaylists().add(new PlayList("Ma playlist", user));
		em.getTransaction().begin();
		em.persist(user);
		em.getTransaction().commit();
	}

	public static void create(PlayList playlist) {
		em.getTransaction().begin();
		em.persist(playlist);
		em.getTransaction().commit();
	}

	public static List<MusicFan> findAllMusicFan() {
		return em.createQuery("SELECT m FROM MusicFan m ORDER BY fullname",
				MusicFan.class).getResultList();
	}

	public static MusicFan findUserByUsername(String username) {
		if (username == null || username.isEmpty()) {
			return null;
		}
		try {
			TypedQuery<MusicFan> query = em.createQuery(
					"SELECT mf FROM MusicFan mf WHERE mf.username = ?1",
					MusicFan.class).setParameter(1, username);
			List<MusicFan> users = query.getResultList();
			if (users.size() == 1) {
				return users.get(0);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static boolean persist(Song song, MusicFan mf, PlayList playlist) {
		try {
			em.getTransaction().begin();
			em.persist(song);
			em.merge(mf);
			em.merge(playlist);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static void setKeywords(Song song, String[] motscles) {
		try {
			Label label;
			List<Label> list;
			for (String mot : motscles) {
				mot = mot.trim();
				list = em
						.createQuery("SELECT l FROM Label l WHERE l.name= ?1",
								Label.class).setParameter(1, mot)
						.getResultList();
				if (list.size() == 1) {
					label = list.get(0);
				} else {
					label = new Label(mot);
					em.getTransaction().begin();
					em.persist(label);
					em.getTransaction().commit();
				}
				if (!song.getLabels().contains(label)) {
					song.getLabels().add(label);
					label.getSongs().add(song);
				}
			}
			em.getTransaction().begin();
			em.merge(song);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static PlayList findPlayListById(long id) {
		return em.find(PlayList.class, id);
	}

	public static Song findSongById(long id) {
		return em.find(Song.class, id);
	}

	public static boolean removeSongFromPlayList(PlayList pl, long songid) {
		em.getTransaction().begin();
		Song song = em.find(Song.class, songid);
		if (song == null || !pl.getSongs().contains(song)) {
			return false;
		}
		pl.getSongs().remove(song);
		em.merge(pl);
		em.getTransaction().commit();
		return true;
	}

	public static boolean addSongToPlayList(PlayList pl, long songid) {
		em.getTransaction().begin();
		Song song = em.find(Song.class, songid);
		if (song == null || pl.getSongs().contains(song)) {
			return false;
		}
		pl.getSongs().add(song);
		em.merge(pl);
		em.getTransaction().commit();
		return true;
	}

	public static MusicFan registerVote(MusicFan user, Song song, int score) {
		em.getTransaction().begin();
		Vote vote = user.getVoteFor(song);
		if (vote == null) {
			vote = new Vote();
			vote.setScore(score);
			vote.setSong(song);
			vote.setVoter(user);
			song.getVotes().add(vote);
			user.getVotes().add(vote);
			em.persist(vote);
		} else {
			vote.setScore(score);
			em.merge(vote);
		}
		em.merge(user);
		em.merge(song);
		em.getTransaction().commit();
		return user;
	}

	public static List<PlayList> findAvailablePlayListFor(MusicFan user) {
		List<PlayList> available = em
				.createQuery(
						"SELECT pl FROM PlayList pl, MusicFan mf WHERE mf = :user AND pl.shared = true AND pl.owner <> mf AND pl NOT MEMBER OF mf.follows",
						PlayList.class).setParameter("user", user)
				.getResultList();
		return available;
	}

	public static boolean share(MusicFan user, long plid, boolean shared) {
		em.getTransaction().begin();
		PlayList playlist = em.find(PlayList.class, plid);
		if (playlist == null) {
			return false;
		}
		if (!user.getPlaylists().contains(playlist)) {
			return false;
		}
		if (shared) {
			playlist.setShared(true);
		} else {
			// remove the playlist from followers
			playlist.setShared(false);
			for (MusicFan mf : playlist.getFollowers()) {
				mf.getFollows().remove(playlist);
			}
			playlist.getFollowers().clear();
		}
		em.getTransaction().commit();
		return true;
	}

	public static boolean follow(MusicFan user, long plid, boolean follows) {
		em.getTransaction().begin();
		PlayList playlist = em.find(PlayList.class, plid);
		if (playlist == null) {
			return false;
		}
		if (!playlist.isShared()) {
			return false;
		}

		if (follows && user.getFollows().contains(playlist)) {
			return false;
		}
		if (!follows && !user.getFollows().contains(playlist)) {
			return false;
		}
		if (follows) {
			user.getFollows().add(playlist);
			playlist.getFollowers().add(user);
		} else {
			user.getFollows().remove(playlist);
			playlist.getFollowers().remove(user);
		}
		em.merge(user);
		em.getTransaction().commit();
		return true;
	}

	public static PlayList findPlayListForUser(MusicFan user, long playlistid) {
		PlayList playlist = em.find(PlayList.class, playlistid);
		if (user.getPlaylists().contains(playlist) || playlist.isShared()) {
			return playlist;
		}
		return null;
	}
}
