<!DOCTYPE HTML>
<html>
<head>
<meta charset='utf-8'> 
<link href="/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="/css/jquery-ui-1.10.1.custom.css" rel="stylesheet" media="screen">
<link href="/css/mine.css" rel="stylesheet" media="screen">
<title>Projet de synthèse DUT 2 2012/2013</title>
</head>
<body>
<script src="/js/jquery-1.9.1.js"></script>
<script src="/js/jquery-ui-1.10.1.custom.js"></script>
<script src="/js/bootstrap.js"></script>
<div class="navbar">
  <div class="navbar-inner">
    <a class="brand">Music OO</a>
     <ul class="nav pull-right">
       <li><a>${user.fullname}</a></li>
       <li><a href="login"><i class="icon-user"></i></a></li>
    </ul>
  </div>
</div>
<div id="upload-form" title="Importer de la musique">  
     <p>Vous pouvez déposer de la musique disponible sur votre machine.</p>
     <p>Attention au format : les mp3 ne sont pas supportés par Firefox, et les fichiers m4a (iTunes) ne sont lisibles que sur Mac</p>
     <p><span class="label label-info">Nouveau</span> La sélection multiple est maintenant disponible !</p>
     <fieldset>
     <form action="/upload" enctype="multipart/form-data" method="post">
       <input type="file" name="musicfile" accept="audio/*" required multiple/>
       <input id="upload-submit" type="submit" value="Importer"/>
     </form>
     </fieldset>
</div>
<script>
$(function() {
   $( "#upload-submit" ).hide();
   $( "#upload-form" ).dialog({
      autoOpen: false,
      height: 350,
      width: 400,
      modal: true,
      buttons: {
        "Importer la musique": function() {
            $( "#upload-submit" ).click();
            $( this ).dialog( "close" );
        },
        Annuler: function() {
          $( this ).dialog( "close" );
        }
      },
      close: function() {
      }
    });
 
    $( "#upload-song" )
      .click(function() {
        $( "#upload-form" ).dialog( "open" );
    });
});
</script>
<div class="container-fluid">
<div class="row-fluid">
<div class="span3">
<h2>Listes de lectures</h2>
<h3>Les miennes</h3>
<#if user.playlists??>
<ul class="unstyled">
<#list user.playlists as pl>
<#if playlist.id == pl.id>
<li class="btn-inverse">
<a href="/managelist/change/${pl_index}/mine">${pl.name}</a>        
<#if pl.shared>
<a href="/social/lock/${pl.id}" title="Cacher"><i class="icon-share icon-white pull-right"></i></a>
<#else>
<a href="/social/share/${pl.id}" title="Partager"><i class="icon-lock icon-white pull-right"></i></a>
</#if>
<a id="upload-song" title="Déposer un fichier musical dans la playlist"><i class="icon-upload icon-white pull-right"></i></a>
<a title="Ajouter un morceau dans la playlist  [A FAIRE]"><i class="icon-list icon-white pull-right"></i></a>
</li>
<#else>
<li>
<a href="/managelist/change/${pl_index}/mine">${pl.name}</a>
<#if pl.shared>
<a href="/social/lock/${pl.id}" title="Cacher"><i class="icon-share pull-right"></i></a>
<#else>
<a href="/social/share/${pl.id}" title="Partager"><i class="icon-lock pull-right"></i></a>
</#if>
</li>
</#if>
<#if pl_has_next><#else>

 <form id="form-create-list" action="/managelist/create" method="post"><input type="text" name="title"/>
 <input type="submit"/></form>
 <a id="bplaylist-create" title="Créer une nouvelle liste de lecture"><i class="icon-plus pull-right"></i></a>
 <script>
$(function() { 
 var button = $( "#bplaylist-create" );
 var form = $( "#form-create-list" );
 form.hide();
 button.click(function() { form.show(); button.hide();});
});
 </script>
</#if>
</#list>
</ul>
</#if>
<h3>Celles que je suis</h3>
<#if user.follows??>
<ul class="unstyled">
<#list user.follows as pl>
<li><a href="/managelist/change/${pl_index}/followed">${pl.name} (${pl.owner})</a> <a href="/follower/remove/${pl.id}" title="ne plus suivre"><i class="icon-remove pull-right"></i></a></li>
</#list>
</ul>
</#if>
<h3>A suivre ... </h3>
<#if proposals??>
<ul class="unstyled">
<#list proposals as pl>
<li><a href="/managelist/change/${pl.id}/proposals"> ${pl.name} (${pl.owner}) </a><a href="/follower/follow/${pl.id}" title="suivre"><i class="icon-check pull-right"></i></a></li>
</#list>
</ul>
</#if>
</div>

<div class="span9">

<#if error??><div class="alert alert-error">
<button type="button" class="close" data-dismiss="alert">&times;</button>
  ${error}
</div>
</#if>
<#if message??><div class="alert alert-success">
<button type="button" class="close" data-dismiss="alert">&times;</button>
  <p>${message}</p>
</div>
</#if>
<#if alecoute??>
<div class="alert alert-info">

<ul class="thumbnails">
  <li class="span6">
    <div class="thumbnail">
    <#if alecoute.artwork??>
      <img src="/artwork/${alecoute.id}" alt="">
    <#else>
      <img id="pochette" src="/img/vinyles-604-564x261.png"/>
    </#if>
    </div>
  </li>  
  <li class="span5">
    <div class="thumbnail">
      <h3>${alecoute.title!"??"}</h3>
      <audio id="myaudioplayer" controls="controls"  autoplay="autoplay"><source src="/song/${alecoute.id}.${alecoute.ext}"/>Pas de support HTML 5 audio</audio>
      <p>Vous écoutez ${alecoute.title!"??"} de ${alecoute.artist!"??"}, de l'album ${alecoute.album}.</p>
      <span id="toreplace"></span>
    </div>
  </li>  
</ul>
</div>
<script>
$.ajax({
  url:"http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=c6b22488b7aa0dc5fd1537b33752295f&artist=${alecoute.artist!""}&track=${alecoute.title!""}&format=json",
  dataType:"json"
}).done(function(data,status,jqXHR) {
  var content ="";
  if (data.track.wiki) {
      content = "<p>"+data.track.wiki.content+"</p>";
  }
  for (i=0; i< data.track.toptags.tag.length ;i++) {
      content = content +" <span class='label'><a href='"+data.track.toptags.tag[i].url+"' class='keyword'>"+data.track.toptags.tag[i].name+"</a></span>";
  }
  $( "#toreplace" ).html("<p>Ecouté "+data.track.playcount+" fois sur <a class='btn' href='"+data.track.url+"'>last.fm</a>.</p>"+content);
  $( "#pochette" ).attr('src',data.track.album.image[3]['#text']);
});
$(function() {
 $( "#nextclickbtn" ).bind('click',function() {
    window.location.href = this.href;
    return false;
 });
 $( "#myaudioplayer" ).bind('ended',function() {
  $( "#nextclickbtn" ).click();
 });
});
</script>
<#else>
<img src="/img/vinyles-604-564x261.png"/>
</#if>
<h1>${playlist.name}
<#if playlist.owner != user>
(${playlist.owner})
</#if>
</h1>
<#if (playlist.songs?size>0)>
<div class="btn-group">    
<#if playlist_index??>
    <#if playlist_index gt 0>
    <a class="btn" href="/playlist/back/${playlist.id}/${playlist_index}"><i class="icon-fast-backward"></i></a>
    <a class="btn" href="/playlist/prev/${playlist.id}/${playlist_index}"><i class="icon-step-backward"></i></a>
    <#else>
    <a class="btn"><i class="icon-fast-backward"></i></a>
    <a class="btn"><i class="icon-step-backward"></i></a>
    </#if>
    <a class="btn" href="/playlist/stop/${playlist.id}/${playlist_index}"><i class="icon-stop"></i></a>
    <a id="nextclickbtn" class="btn" href="/playlist/next/${playlist.id}/${playlist_index}"><i class="icon-step-forward"></i></a>
<#else>   
    <a class="btn"><i class="icon-fast-backward"></i></a>
    <a class="btn"><i class="icon-step-backward"></i></a>
    <a class="btn" href="/playlist/play/${playlist.id}"><i class="icon-play"></i></a> 
    <a class="btn"><i class="icon-step-forward"></i></a>
</#if>
    <a class="btn"><i class="icon-random"></i></a>
    </div>

<table class="table table-striped">
<tr><th>Note</th><th>Nom du morceau</th><th>Artiste</th><th>Album</th><th>Durée</th><th>Actions</th><th>Mots clés</th></tr>
<#list playlist.songs as atrack>
<tr>
 <td>
 <#if user.getVoteFor(atrack)??>
 <#assign vote=user.getVoteFor(atrack)>
 <#list 1..vote.score as score>
 <i class="icon-star"></i>
 </#list>
 <#if vote.score < 5>
 <#list (vote.score+1)..5 as score>
 <i class="icon-star-empty"></i>
 </#list>
 </#if>
 <#else>
 <form id="form-vote-${atrack.id}" action="/vote/${atrack_index}">
 <#list 0..5 as score>
 <input type="radio" name="score" value="${score}"/>${score}
 </#list>
 <input type="submit" value="noter"/>
 </form>
 <a id="bvote-${atrack.id}"><i class="icon-heart"></i></a>
 <script>
$(function() { 
 var button = $( "#bvote-${atrack.id}" );
 var form = $( "#form-vote-${atrack.id}" );
 form.hide();
 button.click(function() { form.show(); button.hide();});
});
 </script>
 </#if>
 </td>
 <td>${atrack.title!atrack.path}</td><td>${atrack.artist!"NA"}</td>
 <td>${atrack.album!"NA"}</td><td>${(atrack.length/60)?int}:${(atrack.length%60)?string("00")}</td>
 <td>
 <#if alecoute??&&alecoute.id==atrack.id>
  <a class="btn btn-primary"><i class="icon-play icon-white"></i></a>
 <#else>
 <a href="/play/${atrack.id}" title="écouter ce morceau"><i class="icon-play"></i></a>
 </#if>
 <#if playlist.owner = user.fullname>
  <a href="/playlist/remove/${playlist.id}/${atrack.id}" title="enlever ce morceau de la playlist"><i class="icon-remove"></i></a>
 </#if>
 </td>
 <td>
 <#if atrack.labels??>
 <span id="tags-${atrack.id}">
 <#list atrack.labels as label>
 <span class="label"><a href="/filter/${atrack_index}/${label_index}" class="keyword">${label.name}</a></span> <#if label_has_next>&nbsp;</#if>
 </#list>
 </span>
 </#if>
 <form id="form-tag-${atrack.id}" action="/motscles/${atrack_index}" method="post"><input type="text" name="motscles" value=
 "<#if atrack.labels??><#list atrack.labels as label>${label.name}<#if label_has_next>,</#if></#list></#if>" placeholder="tag1,tag2,tag3" />
 <input type="submit"/></form>
 <a id="btag-${atrack.id}"><i class="icon-tags"></i></a>
 <script>
$(function() { 
 var button = $( "#btag-${atrack.id}" );
 var form = $( "#form-tag-${atrack.id}" );
 var tags = $( "#tags-${atrack.id}" );
 form.hide();
 button.click(function() { form.show(); button.hide();if (tags) tags.hide();});
});
 </script>
 
 </td>
 </tr>
 </#list>
</table>
<#else>
Pas de fichiers musicaux disponibles. 
</#if>
</div>
</div>
</div>
</body>
</html>