<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>S'identifier</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

  </head>

  <body>
<script src="/js/jquery-1.9.1.js"></script>
<script src="/js/bootstrap.js"></script>
    <div class="container">
<#if error??><div class="alert alert-error">
<button type="button" class="close" data-dismiss="alert">&times;</button>
  ${error}
</div>
</#if>
      <form class="form-signin" action="/loginaction" method="POST">
        <h2 class="form-signin-heading">Merci de vous identifier</h2>
        <input type="text" class="input-block-level" name="username" placeholder="identifiant" required>
        <input type="password" class="input-block-level" name="password" placeholder="mot de passe" required>
        <label class="checkbox">
          <input type="checkbox" name="remember" value="remember-me"> connexion automatique
        </label>
        <button class="btn btn-large btn-primary" type="submit">S'identifier</button>
        <p class="pull-right">Pas de compte ? <a href="/register">enregistrez-vous</a>.</p>
      </form>
    </div> 
  </body>
</html>