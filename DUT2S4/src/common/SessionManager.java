package common;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.simpleframework.http.Cookie;
import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

public final class SessionManager {
	private static final String SESSIONID_NAME = "DUT2MUSICSESSIONID";
	private static final int ID_LENGTH = 14;
	private static Map<String, Session> sessions = new HashMap<String, Session>();
	private static final Random RAND = new Random();

	private SessionManager() {
		// hide default constructor
	}

	public static Session getSession(Request request, Response response) {
		Cookie cookie = request.getCookie(SESSIONID_NAME);
		String sessionId = null;
		if (cookie == null) {
			cookie = response.getCookie(SESSIONID_NAME);
		}
		if (cookie != null) {
			sessionId = cookie.getValue();
		}
		if (sessionId == null) {
			sessionId = generateSessionId();
			response.setCookie(SESSIONID_NAME, sessionId);
		}
		Session session = sessions.get(sessionId);
		if (session == null) {
			session = new Session(sessionId);
			sessions.put(sessionId, session);
		}
		session.setLastAccess(System.currentTimeMillis());
		return session;
	}

	private static String generateSessionId() {
		StringBuffer stb = new StringBuffer();
		for (int i = 0; i < ID_LENGTH; i++) {
			stb.append(RAND.nextInt(10));
		}
		return stb.toString();
	}
}
