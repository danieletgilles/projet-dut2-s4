package common;

import java.io.File;
import java.nio.charset.Charset;

public interface AppConstants {

	Charset UTF8 = Charset.forName("UTF-8");

	int SERVER_PORT = 8082;

	String MUSICDIR_NAME = "DUT2Music";
	File MUSICDIR_FILE = new File(System.getProperty("user.home")
			+ File.separator + MUSICDIR_NAME);
	String DBNAME = "DB";

	// pages
	String LOGIN_PAGE = "login";
	String REGISTER_PAGE = "register";
	String HOME_PAGE = "INDEX";

	// actions
	String LOGIN_ACTION = "loginaction";
	String UPLOAD_ACTION = "upload";
	String KEYWORD_ACTION = "motscles";
	String ARTWORK_ACTION = "artwork";
	String PLAY_ACTION = "play";
	String SONG_ACTION = "song";
	String PLAYLIST_ACTION = "playlist";
	String PLAYLIST_MANAGEMENT_ACTION = "managelist";
	String VOTE_ACTION = "vote";
	String SHARE_ACTION = "social";
	String FOLLOW_ACTION = "follower";
	String FILTER_ACTION = "filter";
	String REGISTER_ACTION = "registeraction";

	// context properties
	String ERROR = "error";
	String MESSAGE = "message";

	// session properties
	String USER = "user";
	String PLAYLIST = "playlist";
	String A_LECOUTE = "alecoute";
	String PLAYLIST_ID = "playlist_index";

	// cookie properties
	String USER_COOKIE = "dut2music";

}
