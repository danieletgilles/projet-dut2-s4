package common;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Session {
	private final String id;

	private final Map<String, Object> attributes = new HashMap<String, Object>();

	private long lastAccess;

	protected Session(String id) {
		this.id = id;
	}

	public void setAttribute(String key, Object value) {
		this.attributes.put(key, value);
	}

	@SuppressWarnings("unchecked")
	public <T> T getAttribute(String key) {
		return (T) this.attributes.get(key);
	}

	public void removeAttribute(String key) {
		this.attributes.remove(key);

	}

	public String getId() {
		return id;
	}

	public long getLastAccess() {
		return lastAccess;
	}

	public void setLastAccess(long lastAccess) {
		this.lastAccess = lastAccess;
	}

	public Set<Map.Entry<String, Object>> attributes() {
		return this.attributes.entrySet();
	}
}
