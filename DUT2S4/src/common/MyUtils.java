package common;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Logger;

public final class MyUtils {
	private static final int BUFFER_SIZE = 10240;

	private MyUtils() {
		// prevent users to create instances of that class.
	}

	public static void copy(InputStream in, OutputStream out) {
		BufferedInputStream bufferedIn = new BufferedInputStream(in);
		BufferedOutputStream bufferedOut = new BufferedOutputStream(out);
		byte[] buffer = new byte[BUFFER_SIZE];
		try {
			int numberOfBytesRead = bufferedIn.read(buffer);
			while (numberOfBytesRead > 0) {
				bufferedOut.write(buffer, 0, numberOfBytesRead);
				numberOfBytesRead = bufferedIn.read(buffer);
			}
			bufferedOut.flush();
		} catch (IOException e) {
			Logger.getAnonymousLogger().severe(e.getMessage());
		}
	}
}
