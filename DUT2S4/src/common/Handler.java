package common;

import java.util.Map;
import java.util.logging.Logger;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import freemarker.template.Configuration;

public interface Handler {
	Logger LOGGER = Logger.getLogger("dut2s4.httpserver");

	String DONE = "DONE";

	String handle(Configuration cfg, Map<String, Object> context,
			Request request, Response response);
}
